package ru.t1.dkozyaikin.tm;

import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.*;
import static ru.t1.dkozyaikin.tm.constant.TerminalConst.*;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        if (processArguments(args)) exit();
        processCommands();
    }

    public static void processCommands() {
        System.out.println("Task Manager started");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nEnter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void processCommand(final String parameter) {
        if (parameter == null || parameter.isEmpty()) {
            displayErrorCommand();
            return;
        }
        switch (parameter) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            case CMD_EXIT:
                exit();
            default: {
                displayErrorCommand();
            }
        }
    }

    public static boolean processArguments(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void processArgument(final String parameter) {
        if (parameter == null || parameter.isEmpty()) {
            displayErrorCommand();
            return;
        }
        switch (parameter) {
            case ARG_HELP:
                displayHelp();
                break;
            case ARG_VERSION:
                displayVersion();
                break;
            case ARG_ABOUT:
                displayAbout();
                break;
            default: {
                displayErrorCommand();
            }
        }
    }

    private static void displayErrorCommand() {
        System.out.println("Error! This command not supported...");
    }

    private static void displayHelp() {
        System.out.printf("%s, %s - displays version - Display program version.\n", CMD_VERSION, ARG_VERSION);
        System.out.printf("%s, %s - display developer info.\n", CMD_ABOUT, ARG_ABOUT);
        System.out.printf("%s, %s - display list of terminal commands.\n", CMD_HELP, ARG_HELP);
        System.out.printf("%s - terminate process.\n", CMD_EXIT);
    }

    private static void displayVersion() {
        System.out.println("1.4.0");
    }

    private static void displayAbout() {
        System.out.println("Denis Kozyaykin");
        System.out.println("dkozyaikin@t1-consulting.ru");
    }

    private static void exit() {
        System.exit(0);
    }

}
